<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ActiveRecordModels\Circle */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Круги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="circle-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту окружность?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'color',
            'radius',
            'text_message:ntext',
            'coord_x',
            'coord_y',
        ],
    ]) ?>

</div>
