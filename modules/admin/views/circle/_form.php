<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ActiveRecordModels\Circle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="circle-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'validationUrl' => Url::toRoute('/admin/circle/validate-circle')
    ]); ?>

    <?= $form->field($model, 'color')->widget(ColorInput::className(), [
        'name' => 'color_33',
        'value' => 'black',
        'showDefaultPalette' => false,
        'options' => ['placeholder' => 'Выберете цвет ...'],
        'pluginOptions' => [
            'showInput' => true,
            'showInitial' => true,
            'showPalette' => true,
            'showPaletteOnly' => true,
            'showSelectionPalette' => true,
            'showAlpha' => false,
            'allowEmpty' => false,
            'preferredFormat' => 'name',
            'palette' => [
                [
                    "white", "black", "grey", "silver", "gold", "brown",
                ],
                [
                    "red", "orange", "yellow", "indigo", "maroon", "pink"
                ],
                [
                    "blue", "green", "violet", "cyan", "magenta", "purple",
                ],
            ]
        ]
    ]) ?>

    <?= $form->field($model, 'text_message')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'coord_x')->textInput() ?>

    <?= $form->field($model, 'coord_y')->textInput() ?>

    <?= $form->field($model, 'radius')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-warning' : 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
