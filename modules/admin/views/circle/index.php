<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ActiveRecordModels\CircleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Круги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="circle-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="text-right">
        <?= Html::a('Создать новую окружность', ['create'], ['class' => 'btn btn-warning']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover table-condensed table-responsive'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'color',
            'radius',
            'text_message:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
