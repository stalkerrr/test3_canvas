Тестовое задание №3: Canvas (сделано на Yii 2.0.10)
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.


[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

Условия задачи
------------

* Написать приложение, используя любой фреймворк. Авторизацию опустить, только два контроллера;
* Приложение состоит из двух частей - фронт-энд и административная;
* Основной функционал на фронт-энде - используя HTML5-canvas рисуются круги разного цвета, при щелчке на круг - выдается сообщение alert("<<Сообщение>>") и окружность меняет цвет;
* В админке можно осуществлять операции CRUD над окружностями указывая им координаты центра, радиус, цвет и сообщение (выдаваемое при щелчке), после сохранения окружность попадает на фронт-энд часть.

Требования
------------

* PHP 5.4.0;
* Composer;
* MySQL 5.5;
* Apache 2.4


Установка
------------

Если у вас нет [Composer](http://getcomposer.org/), вы можете установить его [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).


После того, как данный репозиторий будет клонирован, выполните команды для обновления зависимостей Composer:
~~~
php composer.phar self-update
php composer.phar update
~~~

Создайте базу данных для данного проекта. Внесите информацию о ней в config (см. раздел "Конфигурация").

Восстановите структуру таблиц проекта, применив миграции. Для этого необходимо выполнить следующую команду в консоли (находясь в папке с проектом):
~~~
yii migrate
~~~


Конфигурация
-------------

### База данных

Настройте конфигурационный файл `config/db.php` с данными вашего сервера баз данных, установки по умолчанию:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test3canvas',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```