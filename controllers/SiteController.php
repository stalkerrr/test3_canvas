<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ActiveRecordModels\Circle;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', compact('circles'));
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAjaxGetCircles(){
        if(Yii::$app->request->isAjax){
            $circles = new Circle;
            return json_encode($circles->find()->asArray(true)->all());
        }
        return $this->redirect(['/']);
    }
}
