<?php

use yii\db\Migration;

/**
 * Handles the creation of table `circles`.
 */
class m161216_080656_create_circles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%circles}}', [
            'id' => $this->primaryKey(),
            'color' => $this->string()->notNull(),
            'text_message' => $this->text()->notNull(),
            'coord_x' => $this->smallInteger()->unsigned()->notNull(),
            'coord_y' => $this->smallInteger()->unsigned()->notNull(),
            'radius' => $this->smallInteger()->unsigned()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%circles}}');
    }
}
