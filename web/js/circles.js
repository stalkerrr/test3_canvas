/**
 * Created by hda on 16.12.2016.
 */
$(window).on('load', function () {

    $('#circles_run_button').on('click', function (event) {
        event.preventDefault();
        var clickedButton = $(this);

        $.ajax({
            type: "GET",
            url: "/site/ajax-get-circles",
            success: function(msg){

                if(msg == '[]'){
                    var modalWindow = $('#modal_window');
                    setModalWindowText({
                        'modalObject':modalWindow,
                        'header':'Информационное сообщение',
                        'body': 'В базе данных нет кругов'
                    });
                    modalWindow.modal();
                }
                else{
                    sessionStorage.setItem("circles", msg);
                    var circles = $.parseJSON(sessionStorage.circles);
                    drawCanvasCircles(circles);
                    clickedButton.html('Обновить');
                }
            },
            error: function(){
                alert('Ошибка')
            }
        });
    });
    $('#canvas').on('click', function (e) {
        if(sessionStorage.circles){
            var clickedX = e.pageX - this.offsetLeft;
            var clickedY = e.pageY - this.offsetTop;
            var circles = $.parseJSON(sessionStorage.circles);

            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            var resultBodyMessage = '';

            $.each(circles, function () {
                var elementArea = new CircleArea(this.coord_x, this.coord_y, this.radius);
                var test = Math.pow(((this.coord_x-clickedX) * (this.coord_x-clickedX) + (this.coord_y-clickedY) * (this.coord_y-clickedY)), 0.5);
                if(test<=this.radius){
                    drawCircle(ctx, this.coord_x, this.coord_y, this.radius, getRandomColor());
                    resultBodyMessage += '- ' + this.text_message+'</br>';
                }
            });

            if(resultBodyMessage !== ''){
                var modalWindow = $('#modal_window');
                setModalWindowText({
                    'modalObject':modalWindow,
                    'header':'Вы получили следующие сообщения:',
                    'body': resultBodyMessage
                });
                modalWindow.modal();
            }
        }
    });
});

