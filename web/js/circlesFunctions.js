function drawCircle(canvas, x,y,radius,color) {
    canvas.beginPath();
    canvas.arc(x, y, radius, 0, Math.PI*2, false);
    canvas.fillStyle = color;
    canvas.closePath();
    canvas.fill();
}

function getRandomColor(){
    var r=Math.floor(Math.random() * (256));
    var g=Math.floor(Math.random() * (256));
    var b=Math.floor(Math.random() * (256));

    var fc = '#' + r.toString(16) + g.toString(16) + b.toString(16);
    return fc;

}

function setModalWindowText(paramsObj) {
    paramsObj.modalObject.find('#modal_window_title').html(paramsObj.header);
    paramsObj.modalObject.find('#modal_window_body').html(paramsObj.body);
}

function drawCanvasCircles(circles) {
    console.log(circles);
    if($.type(circles) === 'array'){
        var canvas = document.getElementById("canvas");
        $(canvas).fadeOut();
        setTimeout(function(){
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            $.each(circles, function () {
                drawCircle(ctx, this.coord_x, this.coord_y, this.radius, this.color);
            });
            $(canvas).fadeIn();
        }, 500);

    }
}

var CircleArea = function(x, y, radius) {
    this.left = x - radius;
    this.top = y - radius;
    this.right = +x + +radius;
    this.bottom = +y + +radius;
};