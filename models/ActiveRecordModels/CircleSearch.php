<?php

namespace app\models\ActiveRecordModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ActiveRecordModels\Circle;

/**
 * CircleSearch represents the model behind the search form about `app\models\ActiveRecordModels\Circle`.
 */
class CircleSearch extends Circle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'coord_x', 'coord_y', 'radius'], 'integer'],
            [['color', 'text_message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Circle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'coord_x' => $this->coord_x,
            'coord_y' => $this->coord_y,
            'radius' => $this->radius
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'text_message', $this->text_message]);

        return $dataProvider;
    }
}
