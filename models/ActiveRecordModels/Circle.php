<?php

namespace app\models\ActiveRecordModels;

use Yii;

/**
 * This is the model class for table "circles".
 *
 * @property integer $id
 * @property string $color
 * @property string $text_message
 * @property integer $coord_x
 * @property integer $coord_y
 * @property integer $radius
 */
class Circle extends \yii\db\ActiveRecord
{
    public $right;
    public $left;
    public $top;
    public $bottom;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'circles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color', 'text_message', 'coord_x', 'coord_y', 'radius'], 'required'],
            [['top', 'left', 'bottom', 'right'], 'safe'],
            [['color', 'text_message', 'coord_x', 'coord_y', 'radius'], 'trim'],
            [['text_message'], 'string'],
            [['coord_x', 'coord_y', 'radius'], 'integer','max' => 800],
            [['color'], function ($attribute, $params) {
                $colors = [
                    "white", "black", "grey", "silver", "gold", "brown",
                    "red", "orange", "yellow", "indigo", "maroon", "pink",
                    "blue", "green", "violet", "cyan", "magenta", "purple",
                    ];
                if (!in_array($this->$attribute,$colors)) {
                    $this->addError($attribute, 'Цвет должен быть из палитры!');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Цвет',
            'text_message' => 'Текстовое сообщение',
            'coord_x' => 'Координата X',
            'coord_y' => 'Координата Y',
            'radius' => 'Радиус',
        ];
    }
}
