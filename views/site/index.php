<?php
use app\components\widgets\ModalWindowWidget;
/* @var $this yii\web\View */

$this->title = 'Canvas';
?>

<?= ModalWindowWidget::widget(['body' => 'Test', 'header' => 'header'])?>

<div class="site-index">

    <div id="my_jumbotron" class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p class="lead">Тестовое задание №3.</p>

    </div>

    <div class="body-content">

        <div class="row">

            <div class="col-lg-12">
                <div class="jumbotron2 center-block">
                    <a id="circles_run_button" class="btn btn-lg btn-warning center-block" href="#">Вывести круги &raquo;</a>
                </div>
            </div>

        </div>

        <div class="row">

                <canvas id="canvas" class="" width="800" height="800">
                    Ваш браузер не поддерживает canvas!
                </canvas>


        </div>

    </div>
</div>
