<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ActiveRecordModels\Circle */

$this->title = 'Добавить новую окружность';
$this->params['breadcrumbs'][] = ['label' => 'Круги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="circle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
