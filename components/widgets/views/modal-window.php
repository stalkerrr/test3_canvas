<?php
/* @var $body string
 * @var $header string
 */
?>

<div id="modal_window" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div id="modal_window_content" class="modal-content">
            <div id="modal_window_header" class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal_window_title" class="modal-title"><?= $header ?></h4>
            </div>
            <div class="modal-body">
                <p id="modal_window_body"><?= $body ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->